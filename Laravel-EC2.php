<!-- Installing Laravel-->
Step 1: **Install LAMP
Step 2: sudo apt install phpmyadmin php-mbstring
Step 3: sudo nano /etc/apache2/apache2.conf | input this in the last part: Include /etc/phpmyadmin/apache.conf
Step 4: sudo apt update
Step 5: sudo apt install curl php-cli php-mbstring php-curl git unzip
Step 6: cd ~
		curl -sS https://getcomposer.org/installer -o composer-setup.php
Step 7: HASH=55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae
Step 8: php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
Step 9: The output should be "Installer verified"
Step 10: sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
Step 12: composer
Step 13: php artisan serve --host 0.0.0.0

<!-- To View -->
Step 1: Go to your EC2
Step 2: Edit your inbound rule, add all traffic
Step 3: open Public IP Address


<!-- LINK -->
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-18-04#step-2-downloading-and-installing-composer